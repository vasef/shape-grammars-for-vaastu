from Animation import*
import math
import copy
import sys



class RectButton(object):

    def __init__(self,k,s='m'):
    
        [(left,top),(right,bottom)]=k
        self.left=left
        self.top = top
        self.right=right
        self.bottom=bottom
        self.rect=[(left,top),(right,bottom)]
        self.text=s
        self.textCent=[(left/2+right/2,top/2+bottom/2)]
        

    def isClicked(self,(x,y)):
        if self.left<x<self.right and self.top<y<self.bottom:
            return True
        return False


class Wall(object):

    def __init__(self,startpoint=[0,0],endpoint=[0,100],width=1):
        self.sPoint=startpoint
        self.ePoint=endpoint
        self.lW=width

    def moveWallLeft(self,d=1):
        self.sPoint[0] -=d
        self.ePoint[0] -=d

    def moveWallRight(self,d=1):
        self.sPoint[0] +=d
        self.ePoint[0] +=d

    def moveWallUp(self,d=1):
        self.sPoint[1] -=d
        self.ePoint[1] -=d

    def moveWallDown(self,d=1):
        self.sPoint[1] +=d
        self.ePoint[1] +=d

    def updateStartPoint(x,y):
        self.sPoint[0]=x
        self.sPoint[1]=y

    def extendWall(self,other):
        intersection=self.intersectionOfWalls(other)
        if not (intersection == None):
            self.sPoint=intersection
        return None

    def intersectionOfWalls(w1,w2):
        # this function has code reused from 112 project
        [[x1,y1],[x2,y2]]=w1.getPoints()
        [[x3,y3],[x4,y4]]=w2.getPoints()
        if (x1==x2 and x3==x4):
            return None
        elif(x1==x2):
            a2 = (y4-y3)/float(x4-x3)
            b2 = y3 - a2*float(x3)
            # y=a2 x + b2 is the non vertical line
            #@ x=x1  
            return [x1,float(a2*x1 + b2)]

        elif (x3==x4):
            a1 = (y2-y1)/float(x2-x1)
            b1 = y1 - a1*float(x1)

            return [x3,float(a1*x3 + b1)]

        else:
            a1 = (y2-y1)/float(x2-x1)
            b1 = y1 - a1*float(x1) 
            a2 = (y4-y3)/float(x4-x3)
            b2 = y3 - a2*float(x3)
            if a1==a2 : return None

            x0 = -(b1-b2)/(a1-a2)
            y0= a1*x0 + b1

            return (x0,y0)

    def __repr__(self):
        return "[%r,%r]"%(self.sPoint,self.ePoint)

    def __iter__(self):
        return self

    def next(self):
        pass

    def getPoints(self):
        return [self.sPoint,self.ePoint]

    def doConstraints(self):
        # put in constraints
        pass

class Awall(Wall):
    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        sPoint= [left,(top+bot)/2]
        ePoint= [((right+left)/2) - 4.5*self.scale, (top+bot)/2]
        super(Awall,self).__init__(sPoint,ePoint,3)# Awall is of lwt 3
        self.isSelected=False

    def moveWallLeft(self,d=1):pass
        
    def moveWallRight(self,d=1): pass

    def moveWallUp(self,d=10):
        if not self.isSelected:
            return
        super(Awall,self).moveWallUp(d)
        # write constraints here
        self.Bwall.ePoint=self.intersectionOfWalls(self.Bwall)
        self.Cwall.sPoint=self.intersectionOfWalls(self.Cwall)

    def moveWallDown(self,d=10):
        if not self.isSelected:
            return
        
        super(Awall,self).moveWallDown(d)
        # write constraints here
        self.Bwall.ePoint=self.intersectionOfWalls(self.Bwall)
        self.Cwall.sPoint=self.intersectionOfWalls(self.Cwall)

    def linkConstraints(self,Bwall,Cwall):
        self.Bwall=Bwall
        self.Cwall=Cwall

class Bwall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        sPoint= [((right+left)/2) - 4.5*self.scale,top]
        ePoint= [((right+left)/2) - 4.5*self.scale, (top+bot)/2]
        super(Bwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallLeft(self,d=1): 
        if not self.isSelected:
            return
        super(Bwall,self).moveWallLeft(d)
        # write constraints here

        self.Awall.ePoint=max(self.intersectionOfWalls(self.Awall),self.Awall.intersectionOfWalls(self.Cwall))

    def moveWallRight(self,d=1): 
        if not self.isSelected:
            return
        super(Bwall,self).moveWallRight(d)
        # write constraints here 
        self.Awall.ePoint=max(self.intersectionOfWalls(self.Awall),self.Awall.intersectionOfWalls(self.Cwall))
        
    def moveWallUp(self,d=1):pass

    def moveWallDown(self,d=1):pass

    def linkConstraints(self,Awall,Cwall):
        self.Awall=Awall
        self.Cwall=Cwall

class Cwall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        ePoint= [((right+left)/2) - 4.5*self.scale,bot]
        sPoint= [((right+left)/2) - 4.5*self.scale, (top+bot)/2]
        super(Cwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallLeft(self,d=1): 
        if not self.isSelected:
            return
        super(Cwall,self).moveWallLeft(d)
        # write constraints here

        self.Awall.ePoint=max(self.intersectionOfWalls(self.Awall),self.Bwall.intersectionOfWalls(self.Awall))
        self.Dwall.sPoint=self.intersectionOfWalls(self.Dwall)

    def moveWallRight(self,d=1): 
        if not self.isSelected:
            return
        super(Cwall,self).moveWallRight(d)
        # write constraints here 
        self.Awall.ePoint=max(self.intersectionOfWalls(self.Awall),self.Bwall.intersectionOfWalls(self.Awall))
        self.Dwall.sPoint=self.intersectionOfWalls(self.Dwall)

    def moveWallUp(self,d=1):pass

    def moveWallDown(self,d=1):pass

    def linkConstraints(self,Awall,Bwall,Dwall):
        self.Awall=Awall
        self.Bwall=Bwall
        self.Dwall=Dwall

class Dwall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        sPoint= [((right+left)/2) - 4.5*self.scale,((top+bot)/2) + 3.0*self.scale]
        ePoint= [((right+left)/2) + 4.5*self.scale, ((top+bot)/2)+ 3.0*self.scale]
        super(Dwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallLeft(self,d=1):pass
        
    def moveWallRight(self,d=1): pass

    def moveWallUp(self,d=1):
        if not self.isSelected:
            return
        super(Dwall,self).moveWallUp(d)
        # write constraints here
        self.Ewall.sPoint=self.intersectionOfWalls(self.Ewall)

    def moveWallDown(self,d=1):
        if not self.isSelected:
            return
        super(Dwall,self).moveWallDown(d)
        # write constraints here
        self.Ewall.sPoint=self.intersectionOfWalls(self.Ewall)

    def linkConstraints(self,Ewall):
        
        self.Ewall=Ewall

class Ewall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        sPoint= [((right+left)/2), (top+bot)/2 + 3.0*self.scale]
        ePoint= [((right+left)/2),bot]
        
        super(Ewall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallUp(self,d=1):pass

    def moveWallDown(self,d=1):pass

    def moveWallLeft(self,d=1): 
        if not self.isSelected:
            return
        super(Ewall,self).moveWallLeft(d)
        # write constraints here

        
    def moveWallRight(self,d=1): 
        if not self.isSelected:
            return
        super(Ewall,self).moveWallRight(d)
        # write constraints here 
        





    def linkConstraints(self):
        pass

class Fwall(Wall):
    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        ePoint= [((right+left)/2) + 4.5*self.scale,bot]
        sPoint= [((right+left)/2) + 4.5*self.scale, (top+bot)/2]
        
        super(Fwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallUp(self,d=1):pass

    def moveWallDown(self,d=1):pass

    def moveWallLeft(self,d=1): 
        if not self.isSelected:
            return
        super(Fwall,self).moveWallLeft(d)
        # write constraints here
        self.Gwall.sPoint=self.intersectionOfWalls(self.Gwall)
        self.Dwall.ePoint=self.intersectionOfWalls(self.Dwall)

    def moveWallRight(self,d=1): 
        if not self.isSelected:
            return
        super(Fwall,self).moveWallRight(d)
        # write constraints here
        self.Gwall.sPoint=self.intersectionOfWalls(self.Gwall)
        self.Dwall.ePoint=self.intersectionOfWalls(self.Dwall)

    def linkConstraints(self,Gwall,Dwall):
        self.Gwall=Gwall
        self.Dwall=Dwall

class Gwall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12
        [(left,top),(right,bot)]=outterRectBBox
        sPoint= [((right+left)/2) + 4.5*self.scale,((top+bot)/2)]
        ePoint= [right, ((top+bot)/2)]
        super(Gwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallLeft(self,d=1):pass
        
    def moveWallRight(self,d=1): pass

    def moveWallUp(self,d=1):
        if not self.isSelected:
            return
        super(Gwall,self).moveWallUp(d)
        # write constraints here
        self.Fwall.sPoint=self.intersectionOfWalls(self.Fwall)
        self.Hwall.sPoint=self.intersectionOfWalls(self.Hwall)

    def moveWallDown(self,d=1):
        if not self.isSelected:
            return
        super(Gwall,self).moveWallDown(d)
        # write constraints here
        self.Fwall.sPoint=self.intersectionOfWalls(self.Fwall)
        self.Hwall.sPoint=self.intersectionOfWalls(self.Hwall)

    def linkConstraints(self,Fwall,Hwall):
        self.Fwall=Fwall
        self.Hwall=Hwall
        
class Hwall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an A wall for that.
        self.scale=12

        [(left,top),(right,bot)]=outterRectBBox

        ePoint= [right - 3.0* self.scale,bot]
        sPoint= [right - 3.0* self.scale, (top+bot)/2]
        
        super(Hwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallUp(self,d=1):pass

    def moveWallDown(self,d=1):pass

    def moveWallLeft(self,d=1): 
        pass

    def moveWallRight(self,d=1): 
        pass

    def linkConstraints(self):
        pass

class Iwall(Wall):

    def __init__(self,outterRectBBox):
        # takes a outter rect box which signifies plan 
        # and makes an I wall for that.
        self.scale=12
        [(left,top),(right,bot)]=outterRectBBox
        sPoint= [right - 3.0*self.scale,((top+bot)/2) + 3.0*self.scale]
        ePoint= [right, ((top+bot)/2) + 3.0*self.scale]
        super(Iwall,self).__init__(sPoint,ePoint,3)
        self.isSelected=False

    def moveWallLeft(self,d=1):pass
        
    def moveWallRight(self,d=1): pass

    def moveWallUp(self,d=1):
        if not self.isSelected:
            return
        super(Iwall,self).moveWallUp(d)
        # write constraints here


    def moveWallDown(self,d=1):
        if not self.isSelected:
            return
        super(Iwall,self).moveWallDown(d)
        # write constraints here

    def linkConstraints(self):
        pass  

class VastuHouse(Animation):

    def mousePressed(self,event):
        (x,y)=event.x,event.y
        click=''

        for button in self.buttonList:
            if button.isClicked((x,y)):
                click=button.text

        if click==' increase Area':
            if self.areaInput<1600:
                self.areaInput+=50
            self.init()

        if click=='decrease Area':
            if self.areaInput>700:
                self.areaInput-=50
            self.init()


        pass

    def run(self,width=1000,height=600):
        self.areaInput=900
        super(VastuHouse,self).run(width,height)
        

    def init(self):
        super(VastuHouse,self).init()
        self.scale=12 # 1 inch to 1 pixel
        self.consoleWidth=self.width/5
        self.planMargin=0 # temp to be removed
        
        self.w=Wall()
        self.makeWalls()
        self.makeButtons()

    def makeButtons(self):
        self.buttonList=[]

        w,h=self.width,self.height
        bMargin=10 # button margin

        incButLeft=w- self.consoleWidth + bMargin
        incButTop=0 + bMargin
        incButRight= w- bMargin
        incButBottom= incButTop + 50
        incButText= ' increase Area'
        self.buttonList.append(RectButton([(incButLeft,incButTop),
            (incButRight,incButBottom)],incButText))


        decButTop = incButBottom + bMargin
        decButLeft = w- self.consoleWidth + bMargin
        decButRight= w- bMargin
        decButBottom= decButTop + 50
        decButText= 'decrease Area'
        self.buttonList.append(RectButton([(decButLeft, decButTop),
            (decButRight,decButBottom)],decButText))


    def getRectangeDims(self):
        # for a 2bay configuration
        # fixed frontage is at 26ft
        # max length capped at 45ft
        areaSft=self.areaInput
        assert(areaSft<3000) ## to be roemoved or modified
        maxLength=45.0
        fixedFrontage=26.0
        if areaSft<fixedFrontage*maxLength:
            return (areaSft/fixedFrontage,fixedFrontage)
        else:
            return (maxLength,areaSft/maxLength)

    def getOutterRectBBox(self):
        (rWidth,rHeight)= self.getRectangeDims()
        # scale 1px in 1 inch 1 foot is 12px
        rWidth*=self.scale
        rHeight*=self.scale

        # creating a rectangle.        
        cx,cy=(2*self.width/5),self.height/2
        return [(cx- rWidth/2,cy- rHeight/2),(cx+rWidth/2,
            cy+rHeight/2)]

    def makeWalls(self):
        
        # outter rect as walls.
        [(left,top),(right,bot)]=self.getOutterRectBBox()
        eastWallStPoint,eastWallEnPoint=[right,top],[right,bot]
        westWallStPoint,westWallEnPoint=[left,top],[left,bot]

        norWallStPoint,norWallEnPoint=[right,top],[left,top]
        souWallStPoint,souWallEnPoint=[right,bot],[left,bot]

        self.eWall=Wall(eastWallStPoint,eastWallEnPoint,3)
        self.wWall=Wall(westWallStPoint,westWallEnPoint,3)
        self.nWall=Wall(norWallStPoint,norWallEnPoint,3)
        self.sWall=Wall(souWallStPoint,souWallEnPoint,3)

        # getting the other walls 
        #update in _V2
        oBox=self.getOutterRectBBox()
        self.a=Awall(oBox)
        self.b=Bwall(oBox)
        self.c=Cwall(oBox)
        self.d=Dwall(oBox)
        self.e=Ewall(oBox)
        self.f=Fwall(oBox)
        self.g=Gwall(oBox)
        self.h=Hwall(oBox)
        self.i=Iwall(oBox)

        self.a.linkConstraints(self.b,self.c)
        self.b.linkConstraints(self.a,self.c)
        self.c.linkConstraints(self.a,self.b,self.d)
        self.d.linkConstraints(self.e)
        self.e.linkConstraints()
        self.f.linkConstraints(self.g,self.d)
        self.g.linkConstraints(self.f,self.h)
        self.h.linkConstraints()
        self.i.linkConstraints()

        self.wallList=[self.a,self.b,self.c,self.d,self.e,self.f,self.g,self.h,self.i]

        self.a.isSelected=True

    def drawLine(point,angle,length):
        self.canvas.create_line(point, length*math.cos(angle),
            length*math.sin(angle))

    def keyPressed(self,event):

        key=event.keysym
        if(key=='q'):sys.exit()
        elif(key=='Left'):
            for wall in self.wallList:
                if wall.isSelected:
                    wall.moveWallLeft()
        elif(key=='Right'):
            for wall in self.wallList:
                if wall.isSelected:
                    wall.moveWallRight()
        elif(key=='Up'):
            for wall in self.wallList:
                if wall.isSelected:
                    wall.moveWallUp()
        elif(key=='Down'):
            for wall in self.wallList:
                if wall.isSelected:
                    wall.moveWallDown()
        elif(key=='s'):
            self.cycleSelection()

        ######################from previous version.
        # key=event.keysym
        # if(key=='q'):sys.exit()
        # elif(key=='Left'):
        #     # cycle left
        #     # cycleSelection()
        #     self.w.moveWallLeft()
        # elif(key=='Right'):
        #     print 'right'
        #     # cycleSelection()
        #     self.w.moveWallRight()
        # elif(key=='Up'):
        #     self.bWall.moveWallUp()
        #     self.iWall1.extendWall(self.bWall)
        #     self.iWall2.extendWall(self.bWall)
        #     self.iWall1.moveWallRight()
        #     self.iWall2.moveWallLeft()
        #     self.bRWall.ePoint=self.iWall1.sPoint

        # elif(key=='Down'):
        #     self.bWall.moveWallDown()
        #     self.iWall1.extendWall(self.bWall)
        #     self.iWall2.extendWall(self.bWall)
        #     self.iWall1.moveWallLeft()
        #     self.iWall2.moveWallRight()
        #     self.bRWall.ePoint=self.iWall1.sPoint
        # elif(key=='return'):pass
        #     # makeSelection()

    def cycleSelection(self):
        errorCount=0
        for i in xrange(len(self.wallList)):
            if self.wallList[i].isSelected:
                self.wallList[i].isSelected=False
                self.wallList[i-1].isSelected=True
                break

    def redrawAll(self):

        self.drawConsole()
        self.drawPlan()
        self.canvas.create_line(self.w.getPoints(),width=self.w.lW)
        self.drawButtons()
        self.drawText()

    def drawText(self):
        text='Area is: \n'+ str(self.areaInput)

        self.canvas.create_text(self.width - self.consoleWidth + 10, self.height/2, text=text,anchor=NW, font= 'Arial 12 bold')

        text = 'Use <s> key to \ncycle selection\n\nUP,DOWN,LEFT,RIGHT keys\nmove the wall'
        self.canvas.create_text(self.width- 10,self.height - 10,text=text,anchor=SE)

    def drawButtons(self):
        for button in self.buttonList:
            self.canvas.create_rectangle(button.rect,fill='blue')
            self.canvas.create_text(button.textCent,text=button.text)


    def drawConsole(self):

        bBox=(self.width-self.consoleWidth,0,self.width,self.height)
        self.canvas.create_rectangle(bBox,fill='dim gray',width=0)
        # self.canvas.create_rectangle(20,20,50,50,fill='yellow')

    def drawPlan(self):

        # margin line for testing
        m=self.planMargin
        self.canvas.create_rectangle(m,m,4*self.width/5-m,self.height-m)

        # creating the outter rectangle.        
        # self.canvas.create_rectangle(self.getOutterRectBBox())
        self.canvas.create_line(self.eWall.getPoints(),width=self.eWall.lW)
        self.canvas.create_line(self.wWall.getPoints(),width=self.wWall.lW)
        self.canvas.create_line(self.sWall.getPoints(),width=self.sWall.lW)
        self.canvas.create_line(self.nWall.getPoints(),width=self.nWall.lW)


        # draw all walls.
        
        self.canvas.create_line(self.a.getPoints(),width=self.a.lW,fill='black' if not self.a.isSelected else 'red' )
        self.canvas.create_line(self.b.getPoints(),width=self.b.lW,fill='black' if not self.b.isSelected else 'red')
        self.canvas.create_line(self.c.getPoints(),width=self.c.lW,fill='black' if not self.c.isSelected else 'red')
        self.canvas.create_line(self.d.getPoints(),width=self.d.lW,fill='black' if not self.d.isSelected else 'red')
        self.canvas.create_line(self.e.getPoints(),width=self.e.lW,fill='black' if not self.e.isSelected else 'red')
        self.canvas.create_line(self.f.getPoints(),width=self.f.lW,fill='black' if not self.f.isSelected else 'red')
        self.canvas.create_line(self.g.getPoints(),width=self.g.lW,fill='black' if not self.g.isSelected else 'red')
        self.canvas.create_line(self.h.getPoints(),width=self.h.lW,fill='black' if not self.h.isSelected else 'red')
        self.canvas.create_line(self.i.getPoints(),width=self.i.lW,fill='black' if not self.i.isSelected else 'red')
        
        

        
        
        ################################from version v0
        # # make a bisecting wall
        # self.canvas.create_line(self.bWall.getPoints(),width=self.bWall.lW,fill='white')

        # # make a intersecting related wall( bedroom-toilet)
        # self.canvas.create_line(self.iWall1.getPoints(),width=self.iWall1.lW)

        # # make a intersecting related wall (kitchen-toilet2)
        # self.canvas.create_line(self.iWall2.getPoints(),width=self.iWall2.lW)

        # # make a intersecting related wall (toilet - corridor)
        # self.canvas.create_line(self.bRWall.getPoints(),width=self.bRWall.lW)
        
        # # make a 




        
        # # print w

VastuHouse().run()
