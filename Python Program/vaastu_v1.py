from Animation import*
import math
import sys

class Wall(object):

    def __init__(self,startpoint=[0,0],endpoint=[0,100],width=1):
        self.sPoint=startpoint
        self.ePoint=endpoint
        self.lW=width

    def moveWallLeft(self,d=1):
        self.sPoint[0] -=d
        self.ePoint[0] -=d
        self.doConstraints()

    def moveWallRight(self,d=1):
        self.sPoint[0] +=d
        self.ePoint[0] +=d
        self.doConstraints()

    def moveWallUp(self,d=1):
        self.sPoint[1] -=d
        self.ePoint[1] -=d
        self.doConstraints()

    def moveWallDown(self,d=1):
        self.sPoint[1] +=d
        self.ePoint[1] +=d
        self.doConstraints()

    def updateStartPoint(x,y):
        self.sPoint[0]=x
        self.sPoint[1]=y

    def extendWall(self,other):
        intersection=self.intersectionOfWalls(other)
        if( intersection !=None):
            self.sPoint=intersection
        return None

    def intersectionOfWalls(w1,w2):
        [[x1,y1],[x2,y2]]=w1.getPoints()
        [[x3,y3],[x4,y4]]=w2.getPoints()
        if (x1==x2 and x3==x4):
            return None
        elif(x1==x2):
            a2 = (y4-y3)/float(x4-x3)
            b2 = y3 - a2*float(x3)
            # y=a2 x + b2 is the non vertical line
            #@ x=x1  
            return [x1,float(a2*x1 + b2)]

        elif (x3==x4):
            a1 = (y2-y1)/float(x2-x1)
            b1 = y1 - a1*float(x1)

            return [x3,float(a1*x3 + b1)]

        else:
            a1 = (y2-y1)/float(x2-x1)
            b1 = y1 - a1*float(x1) 
            a2 = (y4-y3)/float(x4-x3)
            b2 = y3 - a2*float(x3)
            if a1==a2 : return None

            x0 = -(b1-b2)/(a1-a2)
            y0= a1*x0 + b1

            return [x0,y0]

    def __repr__(self):
        return "[%r,%r]"%(self.sPoint,self.ePoint)

    def __iter__(self):
        return self

    def next(self):
        pass

    def getPoints(self):
        return [self.sPoint,self.ePoint]

    def doConstraints(self):
        # put in constraints
        pass

# class brHallWall(wall):
#     def 



class VastuHouse(Animation):

    def mousePressed(self):
        pass

    def run(self,width=1000,height=600):
        super(VastuHouse,self).run(width,height)


    def init(self):
        super(VastuHouse,self).init()
        self.scale=12 # 1 inch to 1 pixel
        self.consoleWidth=self.width/5
        self.planMargin=0 # temp to be removed
        self.areaInput= self.getCondoArea()
        self.w=Wall()
        self.makeWalls()
        


    def getCondoArea(self):
        # a=raw_input('enter area of rectange:')
        a=1100
        return a

    def getRectangeDims(self):
        # for a 2bay configuration
        # fixed frontage is at 26ft
        # max length capped at 45ft
        areaSft=self.areaInput
        assert(areaSft<3000) ## to be roemoved or modified
        maxLength=45.0
        fixedFrontage=26.0
        if areaSft<fixedFrontage*maxLength:
            return (areaSft/fixedFrontage,fixedFrontage)
        else:
            return (maxLength,areaSft/maxLength)

    def getOutterRectBBox(self):
        (rWidth,rHeight)= self.getRectangeDims()
        # scale 1px in 1 inch 1 foot is 12px
        rWidth*=self.scale
        rHeight*=self.scale

        # creating a rectangle.        
        cx,cy=(2*self.width/5),self.height/2
        return [(cx- rWidth/2,cy- rHeight/2),(cx+rWidth/2,
            cy+rHeight/2)]

    def makeWalls(self):
        # outter rect as walls.
        [(left,top),(right,bot)]=self.getOutterRectBBox()
        eastWallStPoint,eastWallEnPoint=[right,top],[right,bot]
        westWallStPoint,westWallEnPoint=[left,top],[left,bot]

        norWallStPoint,norWallEnPoint=[right,top],[left,top]
        souWallStPoint,souWallEnPoint=[right,bot],[left,bot]

        self.eWall=Wall(eastWallStPoint,eastWallEnPoint,3)
        self.wWall=Wall(westWallStPoint,westWallEnPoint,3)
        self.nWall=Wall(norWallStPoint,norWallEnPoint,3)
        self.sWall=Wall(souWallStPoint,souWallEnPoint,3)


        # 
        [(left,top),(right,bot)]=self.getOutterRectBBox()
        sPoint,ePoint= [left,(top+bot)/2],[right,(top+bot)/2]
        self.bWall=Wall(sPoint,ePoint,0)

        # 
        [(left,top),(right,bot)]=self.getOutterRectBBox()
        sPoint,ePoint= [(left+right)/3,top],[(left+right)/3,bot]
        self.iWall1=Wall(sPoint,ePoint,width=3)
        self.iWall1.extendWall(self.bWall)


        # 
        [(left,top),(right,bot)]=self.getOutterRectBBox()
        sPoint,ePoint= [2*(left+right)/3,top],[2*(left+right)/3,bot]
        self.iWall2=Wall(sPoint,ePoint,width=3)
        self.iWall2.extendWall(self.bWall)

        # bRWall
        [(left,top),(right,bot)]=self.getOutterRectBBox()
        sPoint=self.bWall.getPoints()[0]
        ePoint=self.iWall1.getPoints()[0]
        self.bRWall=Wall(sPoint,ePoint,3)
 

    def drawLine(point,angle,length):
        self.canvas.create_line(point, length*math.cos(angle),
            length*math.sin(angle))

    def keyPressed(self,event):
        key=event.keysym
        if(key=='q'):sys.exit()
        elif(key=='Left'):
            # cycle left
            # cycleSelection()
            self.w.moveWallLeft()
        elif(key=='Right'):
            print 'right'
            # cycleSelection()
            self.w.moveWallRight()
        elif(key=='Up'):
            self.bWall.moveWallUp()
            self.iWall1.extendWall(self.bWall)
            self.iWall2.extendWall(self.bWall)
            self.iWall1.moveWallRight()
            self.iWall2.moveWallLeft()
            self.bRWall.ePoint=self.iWall1.sPoint

        elif(key=='Down'):
            self.bWall.moveWallDown()
            self.iWall1.extendWall(self.bWall)
            self.iWall2.extendWall(self.bWall)
            self.iWall1.moveWallLeft()
            self.iWall2.moveWallRight()
            self.bRWall.ePoint=self.iWall1.sPoint
        elif(key=='return'):pass
            # makeSelection()

    def redrawAll(self):

        self.drawConsole()
        self.drawPlan()

        self.canvas.create_line(self.w.getPoints(),width=self.w.lW)

    def drawConsole(self):

        bBox=(self.width-self.consoleWidth,0,self.width,self.height)
        self.canvas.create_rectangle(bBox,fill='dim gray',width=0)
        # self.canvas.create_rectangle(20,20,50,50,fill='yellow')

    def drawPlan(self):

        # margin line for testing
        m=self.planMargin
        self.canvas.create_rectangle(m,m,4*self.width/5-m,self.height-m)


        # creating the outter rectangle.        
        # self.canvas.create_rectangle(self.getOutterRectBBox())
        self.canvas.create_line(self.eWall.getPoints(),width=self.eWall.lW)
        self.canvas.create_line(self.wWall.getPoints(),width=self.wWall.lW)
        self.canvas.create_line(self.sWall.getPoints(),width=self.sWall.lW)
        self.canvas.create_line(self.nWall.getPoints(),width=self.nWall.lW)


        # make a bisecting wall
        self.canvas.create_line(self.bWall.getPoints(),width=self.bWall.lW,
            fill='black')

        # make a intersecting related wall
        self.canvas.create_line(self.iWall1.getPoints(),width=self.iWall1.lW)

        # make a intersecting related wall
        self.canvas.create_line(self.iWall2.getPoints(),width=self.iWall2.lW)

        # make a intersecting related wall
        self.canvas.create_line(self.bRWall.getPoints(),width=self.bRWall.lW)
        




        
        # print w



# class Wall(object):
#   def __init__(self,startpoint=(0,0),endpoint=(1,1)):
#       self.sPoint=startpoint
#       self.ePoint=endpoint

#   def moveWallLeft(self,d=1):
#       self.sPoint[0] -= d
#       self.ePoint[0] -= d

#   def moveWallRight(self,d=1):
#       self.sPoint[0] += d
#       self.ePoint[0] += d

#   def moveWallUp(self,d=1):
#       self.sPoint[1] -=d
#       self.ePoint[1] -=d

#   def moveWallDown(self,d=1):
#       self.sPoint[1] +=d
#       self.ePoint[1] +=d

#   def __repr__(self):
#       return [self.sPoint,self.ePoint]

#   def extendWall(self):
#       return None

#   def doConstraints(self):
#       # put in constraints
#       pass

VastuHouse().run()
